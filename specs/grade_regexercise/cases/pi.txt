From http://www.conquermaths.com/news/post/index/211/Pi-versus-Tau-The-Big-Smackdown

You've probably not been able to avoid the fact that today is Pi day, the annual celebration of the mathematical constant Pi - the ratio of the circumference of a circle to its diameter - which is approximately 3.14159. It is an irrational and transcendental number, which continues infinitely without repetition or pattern. This year is particularly impressive, as it is 2015, so the date is ( in the American way of writing the date) 3/14/15 - corresponding to the first five digits of Pi! But what you may not know is that Pi may be in danger of becoming extinct!

Pi has been known to us since the ancient Egyptians and Babylonians and even mentioned in the Old Testament (1 Kings 7:23), when a circular pool is referred to as being 30 cubits around, and 10 cubits across. Pi has been a supremely important number throughout history and is still used constantly.



Archimedes in ancient Greece estimated it to between 3.1408 and 3.1429 and today we have now calculated Pi to over ten trillion decimal places (it only took computer scientists 271 days to calculate that!).

There are Pi memorisation competitions all around the world, and every year anyone with a love of maths celebrates this incredible number today with Pi parades, parties, sky writing and there is even a Pi shrine in San Francisco. Most people, however simply celebrate by eating pie. Mmmmm... Pi.



Yet there is a small but growing discontent among the mathematical community that insists Pi has outlived its usefulness and should be replaced by "Tau".

Never heard of it? It is essentially twice Pi or 6.28. Highly respected mathematicians are putting forward compelling arguments for its superiority, and they want the change implemented not only among the mathematical community but also in education including changing all textbooks, lessons etc.

If so we'd have to make some changes to the ConquerMaths.com content, but that wouldn't be much of a problem, we're good at rolling with the punches (you have to be when you work in education!). However, since great mathematical minds can't agree, we are going to sit firmly on the fence on this one, and while we will give you some of the arguments for Tau, we will let you decide.

The online debate is reignited every Pi Day, particularly since June the 28th (6/28) has been now designated Tau day. M.I.T further fanned the flames by releasing their admissions decisions on Pi Day - but at Tau time - 6:28.
