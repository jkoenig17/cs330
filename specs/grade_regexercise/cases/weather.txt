3:39 pm
Mon, Jan 9	

20 °F	Snow. Ice fog.	9 mph	↑	92%	29.97 "Hg	0 mi
3:31 pm	Snow. Ice fog.	20 °F	Snow. Ice fog.	12 mph	↑	89%	29.98 "Hg	0 mi
3:12 pm	Snow. Ice fog.	20 °F	Snow. Ice fog.	9 mph	↑	89%	29.98 "Hg	0 mi
2:55 pm	Light snow. Mostly cloudy.	21 °F	Light snow. Mostly cloudy.	12 mph	↑	81%	30.03 "Hg	1 mi
1:56 pm	

21 °F	Mostly cloudy.	10 mph	↑	74%	30.09 "Hg	8 mi
12:56 pm	

20 °F	Overcast.	12 mph	↑	78%	30.13 "Hg	8 mi
11:56 am	Partly sunny.	20 °F	Partly sunny.	6 mph	↑	78%	30.18 "Hg	8 mi
10:56 am	

17 °F	Passing clouds.	No wind	↑	83%	30.24 "Hg	7 mi
9:56 am	Sunny.	14 °F	Sunny.	No wind	↑	87%	30.24 "Hg	8 mi
8:56 am	Sunny.	10 °F	Sunny.	No wind	↑	88%	30.20 "Hg	9 mi
7:56 am	Sunny.	10 °F	Sunny.	No wind	↑	88%	30.20 "Hg	9 mi
6:56 am	Clear.	8 °F	Clear.	No wind	↑	91%	30.21 "Hg	9 mi
5:56 am	Clear.	10 °F	Clear.	3 mph	↑	92%	30.19 "Hg	10 mi
4:56 am	Overcast.	13 °F	Overcast.	No wind	↑	81%	30.22 "Hg	10 mi
3:56 am	Overcast.	14 °F	Overcast.	No wind	↑	80%	30.22 "Hg	10 mi
3:03 am	Overcast.	15 °F	Overcast.	No wind	↑	77%	30.16 "Hg	10 mi
2:56 am	Mostly cloudy.	15 °F	Mostly cloudy.	No wind	↑	77%	30.22 "Hg	10 mi
2:35 am	Overcast.	14 °F	Overcast.	No wind	↑	80%	30.16 "Hg	10 mi
1:56 am	Mostly cloudy.	13 °F	Mostly cloudy.	No wind	↑	81%	30.22 "Hg	10 mi
1:27 am	Overcast.	13 °F	Overcast.	No wind	↑	81%	30.16 "Hg	10 mi
12:56 am	Overcast.	13 °F	Overcast.	No wind	↑	81%	30.23 "Hg	10 mi
11:56 pm
Sun, Jan 8	Mostly cloudy.	13 °F	Mostly cloudy.	No wind	↑	74%	30.23 "Hg	10 mi
11:11 pm	Light snow. Mostly cloudy.	12 °F	Light snow. Mostly cloudy.	3 mph	↑	77%	30.16 "Hg	7 mi
10:56 pm	Light snow. Mostly cloudy.	12 °F	Light snow. Mostly cloudy.	8 mph	↑	73%	30.22 "Hg	3 mi
10:33 pm	Light snow. Overcast.	12 °F	Light snow. Overcast.	6 mph	↑	70%	30.19 "Hg	2 mi
10:24 pm	Light snow. Overcast.	12 °F	Light snow. Overcast.	6 mph	↑	70%	30.20 "Hg	3 mi
9:56 pm	

13 °F	Light snow. Overcast.	3 mph	↑	64%	30.30 "Hg	9 mi
8:56 pm	Overcast.	12 °F	Overcast.	7 mph	↑	61%	30.27 "Hg	10 mi
7:56 pm	Overcast.	12 °F	Overcast.	7 mph	↑	58%	30.30 "Hg	10 mi
6:56 pm	Overcast.	12 °F	Overcast.	12 mph	↑	58%	30.30 "Hg	10 mi
5:56 pm	Overcast.	12 °F	Overcast.	15 mph	↑	58%	30.32 "Hg	10 mi
4:56 pm	Mostly cloudy.	11 °F	Mostly cloudy.	10 mph	↑	56%	30.37 "Hg	10 mi
