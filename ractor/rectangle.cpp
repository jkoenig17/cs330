#include<iostream>
#include<stdlib>
#include "Rectangle.h"
using namespace std;

class Rectangle {
  int left;
  int right;
  int top;
  int bottom;
}

  //Constructors
Rectangle::Rectangle (int left, int right, int top, int bottom) {
  this.left = left;
  this.right = right;
  this.top = top;
  this.bottom = bottom;
  }

  // Methods
const int Rectangle::GetLeft(){
  return this.left;  
}

const int Rectangle::GetRight(){
  return this.right;
}

const int Rectangle::GetTop(){
  return this.top;
}

const int Rectangle::GetBottom(){
  return this.bottom;
}

void Rectangle::SetLeft(int left){
  this.left = left
}

void Rectangle::SetRight(int right){
  this.right = right;
}

void Rectangle::SetTop(int top){
  this.top = top;
}

void Rectangle::SetBottom(int bottom){
  this.bottom = bottom;
}

const int Rectangle::GetWidth(){
  return (this.right-this.left);
}

const int Rectangle::GetHeight(){
  return (this.top-this.bottom);
}

const bool Rectangle::Contains(int x, int y){
  if(x>this.right && x<this.left && y>this.bottom && y<this.top) {
    return true;
  }
  return false;
}

bool Rectangle::Intersects(const Rectangle rect){
  if(rect.GetLeft() < this.right && rect.getRight() > this.left && rect.GetTop() < this.bottom && rect.GetBottom > this.top){
    return true;
  }

  return false;
}
