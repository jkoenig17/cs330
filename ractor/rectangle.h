#ifndef RECTANGLE_H
#define RECTANGLE_H

class Rectangle {

private:
	int left;
	int right;
	int top;
	int bottom;
public:
	Rectangle(int left, int right, int top, int bottom);
	const int GetLeft();
	const int GetRight();
	const int GetTop();
	const int GetBottom();
	void SetLeft(int left);
	void SetRight(int right);
	void SetTop(int top);
	void SetBottom(int bottom);
	const int GetWidth();
	const int GetHeight();
	const bool Contains(int x, int y);
	bool Intersects(const Rectangle rect);
}
